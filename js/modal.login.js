		    //Tampilkan Modal 
			function showModalsk( id )
			{
				clearModalsk();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "POST",
						url: "views/crud.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							// waitingDialog.hide();
							setModalDatak( res );
						}
					});
				}
				// Untuk Tambahkan Data
				else
				{
					$("#FormLogin").modal("show");
					$("#myModalLabel").html("Schedule Baru");
					$("#type").val("new"); 
					// waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalDatak( data )
			{
				$("#myModalLabel").html("Login Pegawai");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#user").val(data.user);
				$("#pass").val(data.pass);
				$("#FormLogin").modal("show");
			}

	
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalsk()
			{
				$("#id").val("").removeAttr( "disabled" );
				$("#user").val("").removeAttr( "disabled" );
				$("#pass").val("").removeAttr( "disabled" );
				$("#type").val("");
			}
function cekpass2() {
	var username = $("#user").val();
	var password = $("#pass").val();
	var idname = $("#id").val();
	if(username==''){
		$('#notif').html('<div class="alert alert-danger">Password salah</div>');
		return;
	}
	var myArr;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			myArr = JSON.parse(xmlhttp.responseText);
			if(myArr[0].toString()=='Sukses'){
				$('#notif').html('<div class="alert alert-success">Login sukses</div>');
				setTimeout(function(){window.location.reload(1);}, 800);
			}else{
				$('#notif').html('<div class="alert alert-danger">Password salah</div>');
			}	
		}
		}
		  xmlhttp.open("GET","views/cek.php?username="+username+"&password="+password+"&idname="+idname,true);
		  xmlhttp.send();
}
function cekAdm() {
	var username = $("#users").val();
	var password = $("#passw").val();
	if(username==''){
		$('#notifa').html('<div class="alert alert-danger">Isi dahulu username</div>');
		return;
	}else if(password==''){
		$('#notifa').html('<div class="alert alert-danger">Isi dahulu password</div>');
		return;
	}
	var myArr;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			myArr = JSON.parse(xmlhttp.responseText);
			if(myArr[0].toString()=='Sukses'){
				$('#notifa').html('<div class="alert alert-success">Login sukses</div>');
				setTimeout(function(){window.location.reload(1);}, 800);
			}else{
				$('#notifa').html('<div class="alert alert-danger">Username/Password salah</div>');
				
			}	
		}
		}
		  xmlhttp.open("GET","views/cek_admin.php?username="+username+"&password="+password,true);
		  xmlhttp.send();
}	
	function logout(){
		var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					myArr = JSON.parse(xmlhttp.responseText);
					if(myArr[0] == "ada"){
						$('.alert').html(myArr[1]).delay(200).fadeIn().delay(800).fadeOut();
						$(".alert").addClass("alert-danger");
						setTimeout(function(){window.location.reload(1);}, 800);
					}else{
						$('.alert').html('sukses').delay(200).fadeIn().delay(800).fadeOut();
						$(".alert").addClass("alert-success");
						setTimeout(function(){window.location.reload(1);}, 800);
					}
				}
				}
			xmlhttp.open("GET","views/logout.php?type=logout",true);
			xmlhttp.send();	
	}
$('.modal').on('shown.bs.modal', function() {
  $(this).find('[autofocus]').focus();
});
