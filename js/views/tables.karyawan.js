$(function(){
	// var table;
	table = $('#jsontable').DataTable( {
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": false,
		"bInfo": false,
		"bAutoWidth": false,
        "processing": true,
        "serverSide": true,
        "sAjaxSource": "views/pages/data.karyawan.php"
    } );
});
//Tampilkan Modal 
			function showModals( id )
			{
				// waitingDialog.show();
				clearModalsk();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "GET",
						url: "views/pages/crud.karyawan.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							// waitingDialog.hide();
							setModalData( res );
						}
					});
				}
				// Untuk Tambahkan Data
				else
				{
					$("#FormData").modal("show");
					$("#myModalLabel").html("Data Baru");
					$("#type").val("new"); 
					$("#username").val("").removeAttr( "readonly" );
					// waitingDialog.hide();
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalData(data)
			{
				$("#myModalLabel").html("Data Pegawai");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#username").val(data.user).attr("readonly","true");
				$("#nama").val(data.nama);
				$("#telp").val(data.telp);
				$("#divisi").val(data.divisi);
				$("#aktif").val(data.aktif);
				$("#FormData").modal("show");
			}
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUserp()
			{
			var user = $("#username").val();
			var pass = $("#password").val();
			var nama = $("#nama").val();
			var telp = $("#telp").val();
			var divisi = $("#divisi").val();
			if(user==''){
			$('#notifa').html('Username Masih kosong').delay(200).fadeIn();
			}else if(password==''){
			$('#notifa').html('Password Masih kosong').delay(200).fadeIn();
			}else if(nama==''){
			$('#notifa').html('Nama Masih kosong').delay(200).fadeIn();
			}else if(telp==''){
			$('#notifa').html('No. Telp Masih kosong').delay(200).fadeIn();
			}else if(divisi==''){
			$('#notifa').html('Divisi Belum dipilih').delay(200).fadeIn().delay(3000).fadeOut();
			}else{
				var formData = $("#formUserp").serialize();
				$.ajax({
					type: "GET",
					url: "views/pages/crud.karyawan.php",
					dataType: 'json',
					data: formData,
					beforeSend: function()
					{	
					$("#notifa").fadeOut();
					$("#btn-submit").html('<span class="icon-arrow-right"></span> &nbsp; proses ...');
					},
					success: function(data) {
					if(data=="OK"){
						table.ajax.reload();
						$('#FormData').modal('hide');
						$("#btn-submit").html('Masuk');
					}else{
					$("#notifa").html(data).delay(200).fadeIn();
					$("#removeWarning").hide();
					$("#btn-submit").html('Masuk');
					}
					}
				});
			}
			}
			//Hapus Data
			function deleteUserp(id)
			{
				clearModalsk();
				$.ajax({
					type: "GET",
					url: "views/pages/crud.karyawan.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$("#removeWarning").show();
						$("#notifa").hide();
						$("#myModalLabel").html("Hapus Data");
						$("#id").val(data.id);
						$("#username").val(data.user).attr("disabled","true");
						$("#nama").val(data.nama).attr("disabled","true");
						$("#telp").val(data.telp).attr("disabled","true");
						$("#divisi").val(data.divisi).attr("disabled","true");
						$("#aktif").val(data.aktif).attr("disabled","true");
						$("#type").val("delete");
						$("#FormData").modal("show");		
					}
				});
			}
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalsk()
			{
				$("#removeWarning").hide();
				$("#notifa").hide();
				$("#id").val("").removeAttr( "disabled" );
				$("#username").val("").removeAttr( "disabled" );
				$("#nama").val("").removeAttr( "disabled" );
				$("#telp").val("").removeAttr( "disabled" );
				$("#divisi").val("").removeAttr( "disabled" );
				$("#aktif").val("").removeAttr( "disabled" );
				$("#type").val("");
			}