$(function(){
	// var table;
	table = $('#jsontable').DataTable( {
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": false,
		"bInfo": false,
		"bAutoWidth": false,
        "processing": true,
        "serverSide": true,
        "sAjaxSource": "views/pages/data.divisi.php"
    } );
});
//Tampilkan Modal 
			function showModals( id )
			{
				clearModalsk();
				
				// Untuk Eksekusi Data Yang Ingin di Edit atau Di Hapus 
				if(id)
				{
					$.ajax({
						type: "GET",
						url: "views/pages/crud.divisi.php",
						dataType: 'json',
						data: {id:id,type:"get"},
						success: function(res) {
							setModalData( res );
						}
					});
				}
				// Untuk Tambahkan Data
				else
				{
					$("#FormData").modal("show");
					$("#myModalLabel").html("Data Baru");
					$("#type").val("new"); 
				}
			}
			
			//Data Yang Ingin Di Tampilkan Pada Modal Ketika Di Edit 
			function setModalData(data)
			{
				$("#myModalLabel").html("Data Pegawai");
				$("#id").val(data.id);
				$("#type").val("edit");
				$("#nama").val(data.nama);
				$("#FormData").modal("show");
			}
			//Submit Untuk Eksekusi Tambah/Edit/Hapus Data 
			function submitUserp()
			{
			var nama = $("#nama").val();
			if(nama==''){
			$('#notifa').html('Nama Masih kosong').delay(200).fadeIn();
			}else{
				var formData = $("#formUserp").serialize();
				$.ajax({
					type: "GET",
					url: "views/pages/crud.divisi.php",
					dataType: 'json',
					data: formData,
					success: function(data) {
					if(data=="OK"){
						table.ajax.reload();
						$('#FormData').modal('hide');
						// $("#btn-submit").html('Submit');
					}else{
					$("#notifa").html(data).delay(200).fadeIn();
					$("#removeWarning").hide();
					// $("#btn-submit").html('Submit');
					}
					}
				});
			}
			}
			//Hapus Data
			function deleteUserp(id)
			{
				clearModalsk();
				$.ajax({
					type: "GET",
					url: "views/pages/crud.divisi.php",
					dataType: 'json',
					data: {id:id,type:"get"},
					success: function(data) {
						$("#removeWarning").show();
						$("#notifa").hide();
						$("#myModalLabel").html("Hapus Data");
						$("#id").val(data.id);
						$("#nama").val(data.nama).attr("disabled","true");
						$("#type").val("delete");
						$("#FormData").modal("show");		
					}
				});
			}
			//Clear Modal atau menutup modal supaya tidak terjadi duplikat modal
			function clearModalsk()
			{
				$("#removeWarning").hide();
				$("#notifa").hide();
				$("#id").val("").removeAttr( "disabled" );
				$("#nama").val("").removeAttr( "disabled" );
				$("#type").val("");
			}