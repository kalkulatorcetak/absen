var xmlhttp = false;

try {
	xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch (e) {
	try {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	} catch (E) {
		xmlhttp = false;
	}
}

if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
	xmlhttp = new XMLHttpRequest();
}

function tampildata(){
	var user = document.getElementById("iduser").value;
	var tglawal = document.getElementById("tglawal").value;
	var tglakhir = document.getElementById("tglakhir").value;
	// var obj = document.getElementById("pencarian");
	var url='views/absensi/data_gaji.php?iduser='+user+'&tglawal='+tglawal+'&tglakhir='+tglakhir;
	
	xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState < 4) // while waiting response from server
      $("#pencarian").html("<div align ='center'><img src='img/waiting.gif' alt='Loading' /></div>");
    else if (xmlhttp.readyState === 4) {                // 4 = Response from server has been completely loaded.
        if (xmlhttp.status == 200 && xmlhttp.status < 300)  // http status between 200 to 299 are all successful
          respon = xmlhttp.responseText;
		$('#pencarian').append(respon);
    }
}
	xmlhttp.open("GET", url);
	xmlhttp.send(null);
}

//Libur
function getLibur(){
    $.ajax({
        type: 'GET',
        url: 'views/absensi/libur.act.php',
        data: 'action_type=view&'+$("#liburForm").serialize(),
        success:function(html){
            $('#LiburData').html(html);
        }
    });
}
function liburAction(type,id){
    id = (typeof id == "undefined")?'':id;
    var statusArr = {add:"added",edit:"updated",delete:"deleted"};
    var LiburData = '';
    if (type == 'add') {
        LiburData = $("#addForm").find('.form').serialize()+'&action_type='+type+'&id='+id;
    }else if (type == 'edit'){
        LiburData = $("#editForm").find('.form').serialize()+'&action_type='+type;
    }else{
        LiburData = 'action_type='+type+'&id='+id;
    }
    $.ajax({
        type: 'GET',
        url: 'views/absensi/libur.act.php',
        data: LiburData,
        success:function(msg){
            if(msg == 'ok'){
				swal(type, "Data telah berhasil di "+statusArr[type]+"", "success");
                getLibur();
                $('.form')[0].reset();
                $('.formData').slideUp();
            }else{
				swal("Error", "Beberapa masalah terjadi, coba lagi", "error");
            }
        }
    });
}
function editLibur(id){
    $.ajax({
        type: 'GET',
        dataType:'JSON',
        url: 'views/absensi/libur.act.php',
        data: 'action_type=data&id='+id,
        success:function(data){
            $('#idEditL').val(data.id);
            $('#tglEditL').val(data.tgl);
            $('#keteranganEditL').val(data.keterangan);
            $('#editForm').slideDown();
        }
    });
}

//cuti
function getCuti(){
    $.ajax({
        type: 'GET',
        url: 'views/absensi/cuti.act.php',
        data: 'action_type=view&'+$("#cutiForm").serialize(),
        success:function(html){
            $('#CutiData').html(html);
        }
    });
}
function cutiAction(type,id){
    id = (typeof id == "undefined")?'':id;
    var statusArr = {add:"added",edit:"updated",delete:"deleted"};
    var CutiData = '';
    if (type == 'add') {
        CutiData = $("#addFormC").find('.form').serialize()+'&action_type='+type+'&id='+id;
    }else if (type == 'edit'){
		swal("Tanggal tidak boleh kosong");
        CutiData = $("#editFormC").find('.form').serialize()+'&action_type='+type;
    }else{
        CutiData = 'action_type='+type+'&id='+id;
    }
    $.ajax({
        type: 'GET',
        url: 'views/absensi/cuti.act.php',
        data: CutiData,
        success:function(msg){
            if(msg == 'ok'){
				swal(type, "Data telah berhasil di "+statusArr[type]+"", "success");
                getCuti();
                $('.form')[0].reset();
                $('.formData').slideUp();
            } else if(msg == 'err'){
				swal("Error", "Tanggal masih kosong", "error");
            }else{
				swal("Error", "Beberapa masalah terjadi, coba lagi", "error");
            }
        }
    });
}
function editCuti(id){
    $.ajax({
        type: 'GET',
        dataType:'JSON',
        url: 'views/absensi/cuti.act.php',
        data: 'action_type=data&id='+id,
        success:function(data){
            $('#idEdit').val(data.id);
            $('#iduserEdit').val(data.iduser);
            $('#tglEdit').val(data.tgl);
            $('#keteranganEdit').val(data.keterangan);
            $('#editFormC').slideDown();
        }
    });
}
