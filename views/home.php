<?php
session_start();
include ("../class/conn_db.php");
include ("../class/web_function.php");
include ("../class/ip.php");
if (isset($_SESSION['idab']) AND isset($_SESSION['userab'])){
$iduser = $_SESSION['idab'];
$user = $_SESSION['userab'];
$level = $_SESSION['level'];
}else{
$iduser = '0';
$user = 'Guest';
$level = '';
}


?>
<script>
	var userses = <?=$iduser;?>;
    var requireJS = [];
	loadJS(requireJS, "js/timer.js");
	// cek_absen();
</script>


<div class="animated fadeIn">
                <div class="row" id="reset">
                    <div class="col-md-12 col-sm-12 col-xs-12"><div class="alert" style="display:none"></div></div>
				</div>
    <div class="row">
        <div class="col-sm-6 col-lg-4">
			<div class="card">
                <div class="card-block p-0 clearfix">
                    <i class="fa fa-laptop bg-primary p-4 font-2xl mr-3 float-left"></i>
                    <div class="h5 text-primary mb-0 pt-3"><div id="masuk"></div></div>
                    <div class="text-muted text-uppercase font-weight-bold font-xs">Masuk</div>
                </div>
<?php
if ($level !='admin'){
if ($ip >= $range_start && $ip <= $range_end) {
?>
				<button class="btn btn-success btn-lg klik"  id="btn-masuk" onclick="save_masuk();">Masuk Kerja</button>
<?php
}
}
?>
            </div>
        </div>
		<span style="display:none" id="istirahat"></span>
        <div class="col-sm-6 col-lg-4">
			<div class="card">
                <div class="card-block p-0 clearfix">
                    <i class="fa fa-bell bg-danger p-4 font-2xl mr-3 float-left"></i>
                    <div class="h5 text-danger mb-0 pt-3"><span id="pulang"></span></div>
                    <div class="text-muted text-uppercase font-weight-bold font-xs">Pulang</div>
                </div>
<?php
if ($level !='admin'){
if ($ip >= $range_start && $ip <= $range_end) {
?>
  <button class="btn btn-warning btn-lg klik" id="btn-pulang" onclick="save_pulang();">Pulang Kerja</button>
<?php
}
}
?>
            </div>
        </div>
        <!--/.col-->
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                    <table class="table table-responsive table-hover table-outline mb-0 w-100">
                        <thead class="thead-inverse">
                            <tr>
                                <th><div class="small text-white"><span>No</span></div></th>
                                <th><div class="small text-white">Nama</div></th>
                                <th><div class="small text-white">Masuk</div></th>
                                <th><div class="small text-white">Pulang</div></th>
                                <th><div class="small text-white">Divisi</div></th>
                            </tr>
                        </thead>
                        <tbody>
				<?php
$no=1;
$sqln=mysql_query("SELECT 
  user.nama,
  hak_akses.nama AS akses,
  absen.masuk,
  absen.real_masuk,
  absen.real_pulang,
  absen.pulang,
  absen.id_session
FROM
  hak_akses
  INNER JOIN user ON (hak_akses.id_level = user.id_level)
  INNER JOIN absen ON (user.id_user = absen.id_user) WHERE `tgl` > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 27 HOUR) order by akses ASC ");

				while ($rown=mysql_fetch_array($sqln)){
					if($rown['real_pulang']==null){
						$pulang = "-";
					}else{
						$pulang = jam_update($rown['real_pulang']);
					}
				?>
					<tr>
					<td><div class="small text-gray-dark"><?=$no++;?></div></td>
					<td><div class="small text-gray-dark"><span><?=$rown['nama'];?></span></div></td>
					<td><div class="small text-gray-dark"><?=jam_update($rown['real_masuk']);?></div></td>
					<td><div class="small text-gray-dark"><?=$pulang;?></div></td>
					<td><div class="small text-gray-dark"><?=$rown['akses'];?></div></td>
					</tr>
					
<?php } ?>                      
                        </tbody>
                    </table>
            </div>
        </div>
        <!--/.col-->
    </div>
    <!--/.row-->
</div>
