<?php
session_start();

if (isset($_SESSION['idab']) AND isset($_SESSION['userab'])){
include ("../../class/conn_db.php");
$iduser = $_SESSION['idab'];
$user = $_SESSION['userab'];
$level = $_SESSION['level'];
}else{
$iduser = '0';
$user = 'Guest';
$level = '';
}
if ($level=='admin'){
?>

<script>
    var requireJS = [
      "js/jquery.dataTables.min.js",
      "js/DataTablesBS4.js"
    ];
    loadJS(requireJS, "js/views/tables.karyawan.js");
</script>
<div class="animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Data Karyawan
            <div class="card-actions nav">
                <a  href="#" onClick="showModals()">
					<div class="icon-user-follow"></div>
                </a>
            </div>
                </div>
                <div class="card-block">
                    <table id="jsontable" class="table table-bordered table-striped table-md">
                                        <thead>
                                            <tr>
                                                <th style="width:1% !important;" >No</th>
                                                <th>Nama</th>
                                                <th style="width:5%;text-align:center">Aktif</th>
                                                <th style="width:8%;text-align:center">Aksi</th>
                                            </tr>
                                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
</div>
<div class="modal fade" id="FormData" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Login Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
			<div class="alert alert-danger" role="alert" id="notifa">
			</div>
			<div class="alert alert-danger" role="alert" id="removeWarning">
							Anda yakin ingin menghapus data ini
			</div>
			<form id="formUserp">
			<input type="hidden" class="form-control" id="id" name="id">
			<input type="hidden" class="form-control" id="type" name="type">
			<div class="form-group has-feedback">
			<div class="input-group">
				<span class="input-group-btn">
					<button type="button" class="btn btn-primary">
					<i class="fa fa-user"></i>
					</button>
				</span>
				<input id="username" name="username" class="form-control" type="text" placeholder="username" required >
			</div>
			</div>
			
			<div class="form-group has-feedback">
			<div class="input-group">
				<span class="input-group-btn">
					<button type="button" class="btn btn-danger">
					<i class="fa fa-lock"></i>
					</button>
				</span>
				<input id="password" name="password" class="form-control" type="password" placeholder="Password" >
			</div>
			</div>

			  <div class="form-group has-feedback">
				<div class="input-group">
				<span class="input-group-btn">
					<button type="button" class="btn btn-primary">
					<i class="fa fa-user"></i>
					</button>
				</span>
				<input id="nama" name="nama" class="form-control" type="text" placeholder="Nama Lengkap" required>
				</span>
				</div>
			  </div>
			  
			  <div class="form-group has-feedback">
				<div class="input-group">
				<span class="input-group-btn">
					<button type="button" class="btn btn-primary">
					<i class="fa fa-phone"></i>
					</button>
				</span>
				<input id="telp" name="telp" class="form-control" type="text" placeholder="No. Telp"  >
				</div>
			  </div>
			  <div class="form-group has-feedback">
			 <select name="divisi" id="divisi" class="form-control" required >
			<?php
			$sql_bhn = mysql_query("SELECT * FROM hak_akses where publish='Y' ORDER BY nama");
			echo "<option value=''>- Pilih Divisi -</option>";
			while($w=mysql_fetch_array($sql_bhn)){
			echo "<option value=$w[id_level]>$w[nama]</option>";
			}
			?>
			</select>
			  </div>
			  <div class="form-group has-feedback">
			 <select name="aktif" id="aktif" class="form-control" required >
				<option value='' selected>-- Pilih --</option>
				<option value='Y'>Active</option>
				<option value='N'>Inactive</option>
			 </select>
			  </div>

			  </form>
            </div>
            <div class="modal-footer">
				<button type="button" id="btn-submit" onClick="submitUserp()" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-warning" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Admin alert</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Admin tidak bisa dihapus</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php 
}else{
include "error.php";
} 
?>