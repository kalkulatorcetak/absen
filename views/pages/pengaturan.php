<?php
session_start();

if (isset($_SESSION['idab']) AND isset($_SESSION['userab'])){
include ("../../class/conn_db.php");
$iduser = $_SESSION['idab'];
$user = $_SESSION['userab'];
$level = $_SESSION['level'];
}else{
$iduser = '0';
$user = 'Guest';
$level = '';
}
if ($level=='admin'){
$result=mysql_query("select * from info where id_info=1");
$data = mysql_fetch_array($result);

$resip=mysql_query("select * from ip where id_ip=1 AND publish='Y'");
$rowip = mysql_fetch_array($resip);
?>
<script>
    var requireJS = [];
	loadJS(requireJS, "js/views/pengaturan.js");
</script>
<div class="animated fadeIn">
 <div class="row">
<div class="col-md-6">
   <div class="card">
                <div class="card-header">
                    <strong>Pengaturan Absen</strong>
                </div>
                <div class="card-block">
				<div id="form-content">
                    <form method="post" id="reg-form" autocomplete="off">
                        <div class="form-group">
                            <label for="nama">Nama Aplikasi</label>
                            <input name="id" class="form-control" value="1" type="hidden">
                            <input id="aplikasi" name="aplikasi" class="form-control" value="<?=$data['nama_aplikasi'];?>" placeholder="Nama Aplikasi" type="text">
                        </div>
						<div class="form-group">
                            <label for="nama">Nama Perusahaan</label>
                            <input name="id" class="form-control" value="1" type="hidden">
                            <input id="nama" name="nama" class="form-control" value="<?=$data['nama_perusahaan'];?>" placeholder="Nama Perusahaan" type="text">
                        </div>
						<div class="form-group">
                            <label for="alamat">Alamat Perusahaan</label>
                            <input id="alamat" name="alamat" class="form-control" value="<?=$data['alamat_perusahaan'];?>" placeholder="Alamat Perusahaan" type="text">
                        </div>
						<div class="form-group">
                            <label for="logo">Email Perusahaan</label>
                            <input id="email" name="email" class="form-control" value="<?=$data['email'];?>" placeholder="Logo Perusahaan" type="email">
                        </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Simpan</button>
                </div>
                    </form>
				</div>
				</div>
	</div>
</div>
<div class="col-md-6">
<div class="card">
                <div class="card-header">
                    Pengaturan
                    <strong>IP</strong>
                </div>
                <div class="card-block">
				<div id="form-ip">
                    <form method="post" id="ip-form" autocomplete="off">
					<div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">IP Start</span>
								<input name="idip" class="form-control" value="1" type="hidden">
                                <input id="ip1" name="ip1" value="<?=$rowip['range_start'];?>" class="form-control" type="text" placeholder="192.168.0.1" maxlength="15">
                            </div>
					</div>
					<div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">IP End</span>
                                <input id="ip2" name="ip2" value="<?=$rowip['range_end'];?>" class="form-control" type="text" placeholder="192.168.0.255" maxlength="15">
                            </div>
					</div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Simpan</button>
                </div>
                    </form>
                </div>
                </div>

            </div>
</div>
</div>
</div>
<?php 
}else{
include "error.php";
} 
?>