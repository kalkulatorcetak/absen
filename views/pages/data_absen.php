<?php
session_start();

if (isset($_SESSION['idab']) AND isset($_SESSION['userab'])){
include ("../../class/conn_db.php");
include ("../../class/web_function.php");


if(isset($_POST['tglawal'])){

	$id_user = $_POST['id_user'];
	$tglawal = date_slash($_POST['tglawal']);
	$tglakhir = date_slash($_POST['tglakhir']);
	$bln = date('m',strtotime($tglakhir));
	$thn = date('Y',strtotime($tglakhir));
	
	if ($tglawal ==""){
		$bln = date('m');
		$thn = date('Y');
		$tglawal = date('Y-m', strtotime('-1 month')) . "-25";
		$tglakhir = $thn . "-" . $bln . "-26";

	}

}elseif(isset($_GET['id_user'])){
	$id_user = $_GET['id_user'];
	$tglawal = date_slash($_GET['tglawal']);
	$tglakhir = date_slash($_GET['tglakhir']);
	$bln = date('m',strtotime($tglakhir));
	$thn = date('Y',strtotime($tglakhir));
}else{	
	$id_user = $_SESSION['idab'];
		$bln = date('m');
		$thn = date('Y');
		$tglawal = date('Y-m') . "-1";
		$tglakhir = date('Y-m-t');
}




	$sql = mysql_query("select nama from user where id_user = '$id_user'");
	$row = mysql_fetch_array($sql);
	$nama = $row['nama'];
	
	
?>
<script>

    var requireJS = [];
	loadJS(requireJS, "js/views/absen.js");
	$('.tglawal,.tglakhir').datepicker({
	format: 'dd/mm/yyyy'
	});	
</script>


<div class="animated fadeIn">
                <div class="row" id="reset">
                    <div class="col-md-12 col-sm-12 col-xs-12"><div class="alert" style="display:none"></div></div>
				</div>

    <!--/.row-->
		<form method="post" action="views/pages/export.php" target="_blank">
						
						    <div class="row">
							<div class="col-md-3 col-sm-3 col-xs-3">
								<?php 									
								$sqld = mysql_query("Select * from user order by nama");
								?>
								<select id="iduser" name="id_user"  class="form-control" >
								<option value="0">- Pilih Nama -</option>
								<?php 
								while($row = mysql_fetch_array($sqld)){ ?>
								<option  value="<?=$row['id_user'];?>"><?=$row['nama'];?></option>
								<?php }
								?>
								</select>
							</div>
							<div class="col-md-5 col-sm-5 col-xs-5">
								<div class="form-group input-group">
								<span class="input-group-addon" >Dari Tanggal</span>
									<input class="form-control text-center tglawal"   id="tglawal" name="tglawal" value="<?=tglsl($tglawal);?>">
							
								<span class="input-group-addon" >Sampai</span>
									<input class="form-control text-center tglakhir"  id="tglakhir" name="tglakhir" value="<?=tglsl($tglakhir);?>">
							</div>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-3">
							<button class="btn btn-primary" onclick="tampildata();" type="button">Tampilkan</button>
								<input  type="submit" name="go" class="btn btn-success" id="btn-export" value="Export XLS">
							</div>	
						</form>
							</div>
		

    <div class="row">
        <div class="col-md-8">
            <div class="card">
			<div id="pencarian"></div>
                    <table class="table table-responsive table-hover table-outline mb-0 w-100">
                        <thead class="thead-inverse">
                            <tr>
                                <th><div class="small text-white"><span>No</span></div></th>
                                <th><div class="small text-white">Nama</div></th>
                                <th><div class="small text-white">Tanggal</div></th>
                                <th><div class="small text-white">Masuk</div></th>
                                <th><div class="small text-white">Pulang</div></th>
                                <th><div class="small text-white">Divisi</div></th>
                            </tr>
                        </thead>
                        <tbody>
				<?php
$no=1;
$sqln=mysql_query("SELECT 
  user.nama,
  hak_akses.nama AS akses,
  absen.masuk,
  absen.tgl,
  absen.real_masuk,
  absen.real_pulang,
  absen.pulang,
  absen.id_session
FROM
  hak_akses
  INNER JOIN user ON (hak_akses.id_level = user.id_level)
  INNER JOIN absen ON (user.id_user = absen.id_user)  order by tgl ASC ");

				while ($rown=mysql_fetch_array($sqln)){
					if($rown['real_pulang']==null){
						$pulang = "-";
					}else{
						$pulang = jam_update($rown['real_pulang']);
					}
				?>
					<tr>
					<td><div class="small text-gray-dark"><?=$no++;?></div></td>
					<td><div class="small text-gray-dark"><span><?=$rown['nama'];?></span></div></td>
					<td><div class="small text-gray-dark"><span><?=tglsl($rown['tgl']);?></span></div></td>
					<td><div class="small text-gray-dark"><?=jam_update($rown['real_masuk']);?></div></td>
					<td><div class="small text-gray-dark"><?=$pulang;?></div></td>
					<td><div class="small text-gray-dark"><?=$rown['akses'];?></div></td>
					</tr>
					
<?php } ?>                      
                        </tbody>
                    </table>
            </div>
        </div>
        <!--/.col-->
    </div>
    <!--/.row-->
</div>
<?php
}else{
include "error.php";
}
?>
