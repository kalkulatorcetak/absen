-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2017 at 08:01 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_absen`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen`
--

CREATE TABLE `absen` (
  `ID` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `real_masuk` datetime DEFAULT NULL,
  `masuk` datetime DEFAULT NULL,
  `istirahat` datetime NOT NULL,
  `real_pulang` datetime DEFAULT NULL,
  `pulang` datetime DEFAULT NULL,
  `id_session` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bayar_gaji`
--

CREATE TABLE `bayar_gaji` (
  `id` int(11) NOT NULL,
  `id_slip` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `jml_bayar` int(11) NOT NULL,
  `id_byr` int(11) NOT NULL,
  `id_user` int(3) NOT NULL,
  `jurnal` enum('Y','N') DEFAULT 'N'
) ENGINE=MyISAM AVG_ROW_LENGTH=39 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `departemen`
--

CREATE TABLE `departemen` (
  `id_departemen` varchar(30) NOT NULL,
  `nama_departemen` varchar(30) NOT NULL,
  `ket_departemen` varchar(30) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `tlp` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `email` varchar(70) NOT NULL,
  `cs` varchar(40) NOT NULL,
  `aktive` enum('Y','N') DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departemen`
--

INSERT INTO `departemen` (`id_departemen`, `nama_departemen`, `ket_departemen`, `alamat`, `tlp`, `fax`, `email`, `cs`, `aktive`) VALUES
('SR', 'SDC Serang', 'sayuti.com', 'Jl. Raya A. Fatah Hasan No. 61 (Depan Taspen) Cijawa Masjid Serang Banten <br> Telp. +6254 221265 Hp. 08777 34567 33 Email: sayuticom@yahoo.com', '0254- 48486360', '0254- 48486585', '', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `ID` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `gaji_pokok` int(11) DEFAULT NULL,
  `tun_jab` int(11) DEFAULT NULL,
  `transport` int(11) DEFAULT NULL,
  `makan` int(11) DEFAULT NULL,
  `asuransi` int(11) DEFAULT NULL,
  `jam_kerja` int(11) DEFAULT NULL,
  `istirahat` int(11) DEFAULT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=37 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `id_level` int(3) NOT NULL,
  `id_parent` int(2) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `level` varchar(20) NOT NULL,
  `publish` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`id_level`, `id_parent`, `nama`, `level`, `publish`) VALUES
(11, 0, 'Administrator', 'admin', 'Y'),
(14, 0, 'Front Office', 'user', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `hari_libur`
--

CREATE TABLE `hari_libur` (
  `ID` int(11) NOT NULL,
  `tgl` date DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=37 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id_info` int(2) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `nama_aplikasi` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(255) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id_info`, `nama_perusahaan`, `nama_aplikasi`, `alamat_perusahaan`, `telp`, `email`) VALUES
(1, 'Percetakan sayuti.com', 'Absensi Karyawan', 'Jl. A. Fatah Hasan No. 61 Cijawa Serang', '+628121200995', 'sayuticom@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `ip`
--

CREATE TABLE `ip` (
  `id_ip` int(2) NOT NULL,
  `range_start` varchar(50) NOT NULL,
  `range_end` varchar(50) NOT NULL,
  `publish` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ip`
--

INSERT INTO `ip` (`id_ip`, `range_start`, `range_end`, `publish`) VALUES
(1, '192.168.43.1', '192.168.43.255', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `izin`
--

CREATE TABLE `izin` (
  `ID` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=37 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon`
--

CREATE TABLE `kasbon` (
  `id_kasbon` int(11) NOT NULL,
  `tgl_kasbon` date DEFAULT NULL,
  `jenis_kasbon` varchar(20) DEFAULT NULL,
  `id_pegawai` int(11) NOT NULL,
  `debet` int(11) NOT NULL,
  `kredit` int(11) NOT NULL,
  `catatan` varchar(50) NOT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=59 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menuadmin`
--

CREATE TABLE `menuadmin` (
  `idmenu` int(5) NOT NULL,
  `idparent` int(5) NOT NULL DEFAULT '0',
  `id_level` varchar(100) NOT NULL,
  `nama_menu` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `target` varchar(10) NOT NULL DEFAULT '_parent',
  `link_on` enum('Y','N') NOT NULL DEFAULT 'Y',
  `treeview` varchar(10) NOT NULL DEFAULT 'treeview',
  `class` varchar(20) NOT NULL,
  `classicon` enum('Y','N') NOT NULL DEFAULT 'Y',
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `level` varchar(100) NOT NULL,
  `urutan` int(5) NOT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=74 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menuadmin`
--

INSERT INTO `menuadmin` (`idmenu`, `idparent`, `id_level`, `nama_menu`, `link`, `target`, `link_on`, `treeview`, `class`, `classicon`, `aktif`, `level`, `urutan`) VALUES
(2, 0, '1,2,3,4,5,6', 'Beranda', 'home', '_parent', 'Y', '', 'th-large', 'N', 'Y', '', 1),
(6, 0, '1', 'PANEL ADMINISTRATOR', 'berita', '_parent', 'N', 'header', 'bars', 'Y', 'Y', 'user', 30),
(20, 0, '5', 'Keuangan', '#', '_parent', 'Y', 'treeview', 'money', 'Y', 'Y', 'admin', 4),
(21, 88, '1,2,3,4,5,6', 'Konsumen', 'konsumen', '_parent', 'Y', '', 'bars', 'N', 'Y', '', 4),
(24, 0, '1', 'Administrator', 'menuadmin', '_parent', 'Y', 'treeview', 'gear', 'N', 'Y', 'admin', 32),
(33, 100, '1,2,3,4,5,6', 'Pengguna', 'user', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', 'admin', 3),
(81, 123, '1,3,5', 'Jurnal Umum', 'jurnal', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 5),
(82, 20, '1,3', 'Daftar Akun', 'akun', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 2),
(83, 123, '1,3,5', 'Buku Besar', 'buku_besar', '_parent', 'Y', 'treeview', 'file-text-o', 'Y', 'Y', '', 5),
(84, 20, '1,3,5', 'Laba Rugi', 'laba_rugi', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 5),
(85, 20, '1,3,6', 'Neraca', 'neraca', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 6),
(86, 20, '1,3,5', 'Aktiva', 'aktiva', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 6),
(88, 0, '1,3,4,5,6', 'Invoice', '#', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 2),
(89, 88, '1,4,5,6', 'Data Invoice', 'invoice', '_parent', 'Y', '', 'bars', 'N', 'Y', '', 1),
(90, 88, '1,3,4,5', 'Rekap Invoice', 'invoice&amp;act=laporan_penjualan', '_parent', 'Y', '', 'bars', 'N', 'Y', 'admin', 4),
(92, 88, '1,2,3,4,5,6', 'Invoice Baru', 'konsumen', '_parent', 'Y', '', 'bars', 'N', 'Y', '', 1),
(93, 20, '1,2,3,4,5,6', 'Tutup Buku', 'rekap_keuangan', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 4),
(94, 88, '1,3,4,5', 'Uang Masuk', 'invoice&amp;act=laporan_uang_masuk', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 9),
(95, 0, '1', 'Setting', 'setting', '_parent', 'Y', 'treeview', 'gears', 'N', 'Y', '', 31),
(99, 0, '1,3,6', 'Grafik', 'chart', '_parent', 'Y', '', 'area-chart', 'N', 'Y', '', 11),
(100, 0, '1,3,5', 'Data-Data', '#', '_parent', 'Y', 'treeview', 'folder-open', 'Y', 'Y', '', 7),
(104, 100, '1', 'History Chat', 'chat', '_parent', 'Y', 'treeview', 'file-text-o', 'N', 'Y', '', 3),
(105, 100, '1', 'Departemen', 'dept', '_parent', 'Y', 'treeview', 'file-text-o', 'Y', 'Y', '', 5),
(106, 88, '1,6', 'Laporan Desain', 'invoice&amp;act=desain', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 6),
(107, 88, '1,2,3,4,5,6', 'Laporan Omset', 'invoice&amp;act=laporan_omset', '_parent', 'Y', '', 'file-text-o', 'Y', 'Y', '', 8),
(108, 88, '1,2,3,4,5', 'Piutang', 'invoice&amp;act=laporan_piutang', '_parent', 'Y', '', 'file-text-o', 'Y', 'Y', '', 10),
(109, 88, '1,2,3,4,5,6', 'Status Kerjaan', 'invoice&amp;act=status_kerjaan', '_parent', 'Y', '', 'bars', 'Y', 'Y', '', 11),
(111, 0, '1,2,3,4,5,6,8', 'Jadwal Kerja', 'jadwal', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 7),
(112, 111, '1,4,6', 'Jadwal Kerja Desain', 'jadwal', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 1),
(113, 111, '1', 'Buat Jadwal', 'jadwal&amp;act=buat', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 2),
(114, 111, '1,4,6', 'Jadwal Kerja FO', 'jadwal&amp;act=fo', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 3),
(115, 100, '1,6', 'Database', 'database', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 6),
(116, 0, '1,3,5', 'Pengeluaran', '#', '_parent', 'Y', 'treeview', 'cart-plus', 'Y', 'Y', '', 3),
(117, 116, '1,5', 'Input Pengeluaran', 'pengeluaran', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 1),
(118, 116, '1,3,5', 'View Pengeluaran', 'pengeluaran&amp;act=view_pengeluaran', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 6),
(119, 116, '1,5', 'Hutang Usaha', 'pengeluaran&amp;act=laporan_hutang', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 10),
(120, 116, '1,5', 'Biaya-biaya', 'biaya', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 10),
(122, 116, '1,5', 'Supplier', 'supplier', '_parent', 'Y', 'treeview', 'user', 'Y', 'Y', '', 15),
(123, 0, '1,5', 'Jurnal', '#', '_parent', 'Y', 'treeview', 'book', 'Y', 'Y', '', 5),
(124, 123, '1,5', 'View Jurnal Umum', 'jurnal&amp;act=view_jurnal', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 10),
(126, 116, '1,5', 'Stok Barang', 'stok', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 1),
(127, 20, '1,5', 'Kasbon Karyawan', 'kasbon', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 15),
(128, 100, '1,5', 'Produk', 'produk', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 20),
(129, 100, '1,5', 'Jenis Cetakan', 'jenis_cetakan', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 21),
(130, 100, '1,5', 'Bahan &amp; dll', 'bahandll', '_parent', 'Y', 'treeview', 'bars', 'Y', 'N', '', 25),
(131, 132, '1,2,8', 'Laporan Outdoor', 'operator', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 12),
(132, 0, '1,2', 'Laporan Operator', '#', '_parent', 'Y', 'treeview', 'list-alt', 'Y', 'Y', '', 6),
(133, 132, '1,8', 'Laporan A3', 'operator&amp;act=A3', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 2),
(134, 132, '1,8', 'Laporan Indoor', 'operator&amp;act=indoor', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 3),
(135, 111, '1,2', 'Jadwal Kerja Outdoor', 'jadwal&amp;act=outdoor', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 4),
(136, 111, '1,8', 'Jadwal Kerja Indoor', 'jadwal&amp;act=indoor', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 5),
(137, 0, '1,3,4,5,6', 'No Antrian', '#', '_parent', 'Y', 'treeview', 'bars', 'N', 'N', '', 6),
(138, 137, '1,3,4,5,6', 'Panel Antrian', 'antrian', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 1),
(139, 137, '1,3,4,5,6', 'Monitor Antrian', 'antrian&amp;act=monitor', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 2),
(140, 137, '1,3,4,5,6', 'Panggil Antrian', 'antrian&amp;act=panggil', '_parent', 'Y', 'treeview', 'bars', 'Y', 'Y', '', 12),
(141, 88, '1,3,4,6', 'Pelunasan', 'cekdata', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 4),
(146, 0, '1,3,5', 'Absensi dan Penggajian', '#', '_parent', 'Y', 'treeview', 'file-text-o', 'Y', 'Y', '', 1),
(147, 146, '1,3,5', 'Form Penggajian', 'absensi&amp;act=form_gajian', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 1),
(148, 146, '1,3,5', 'Absen', 'absensi', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 0),
(149, 146, '1,3,5', 'Koreksi Absen', 'absensi&amp;act=koreksi', '_parent', 'Y', 'treeview', 'bars', 'N', 'Y', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `nama` varchar(20) DEFAULT NULL,
  `folder_data` varchar(20) DEFAULT NULL,
  `harga_desain` int(11) DEFAULT NULL,
  `harga_min` int(11) NOT NULL,
  `dp_minimal` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `status`, `nama`, `folder_data`, `harga_desain`, `harga_min`, `dp_minimal`) VALUES
(1, 'Y', '', 'data', 45000, 15000, 10);

-- --------------------------------------------------------

--
-- Table structure for table `slip_gaji`
--

CREATE TABLE `slip_gaji` (
  `id_slip` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tgl_rekap` date DEFAULT NULL,
  `bulan_gaji` int(11) DEFAULT NULL,
  `tahun_gaji` int(11) DEFAULT NULL,
  `gaji_pokok` int(11) DEFAULT NULL,
  `tun_jab` int(11) DEFAULT NULL,
  `transport` int(11) DEFAULT NULL,
  `makan` int(11) DEFAULT NULL,
  `asuransi` int(11) DEFAULT NULL,
  `jam_kerja` int(11) DEFAULT NULL,
  `istirahat` int(11) DEFAULT NULL,
  `jml_kerja` int(11) DEFAULT NULL,
  `jml_cuti` int(11) DEFAULT NULL,
  `jml_libur` int(11) DEFAULT NULL,
  `gaji_kotor` int(11) DEFAULT NULL,
  `lembur` int(11) DEFAULT NULL,
  `tot_makan` int(11) DEFAULT NULL,
  `tot_transport` int(11) DEFAULT NULL,
  `tot_tun_cuti` int(11) DEFAULT NULL,
  `tot_tun_libur` int(11) DEFAULT NULL,
  `tot_tun_jab` int(11) DEFAULT NULL,
  `tot_bonus` int(11) DEFAULT NULL,
  `pot_absen` int(11) DEFAULT NULL,
  `pot_asuransi` int(11) DEFAULT NULL,
  `pot_kasbon` int(11) DEFAULT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=37 DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(10) NOT NULL,
  `idmenu` text NOT NULL,
  `id_level` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  `level` varchar(20) NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `id_session` varchar(100) NOT NULL,
  `online` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `idmenu`, `id_level`, `username`, `password`, `nama`, `alamat`, `email`, `no_hp`, `level`, `aktif`, `id_session`, `online`) VALUES
(2, '2,146,148,147,149,88,92,89,90,141,21,106,107,94,108,109,116,117,126,118,119,120,122,20,82,93,84,85,86,127,123,81,83,124,132,133,134,131,137,138,100,104,33,105,115,128,129,130,111,112,113,114,135,136,99,6,95,24', 11, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Ahmad Sayuti', 'Banten', 'admin@sayuti.com', '0899828282', 'admin', 'Y', 'pfO2wVGhWs', 'Y'),
(442, '', 14, 'nia', '202cb962ac59075b964b07152d234b70', 'Nia ', '', '', '21415', 'user', 'Y', 'p2jIJrBYWh', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absen`
--
ALTER TABLE `absen`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bayar_gaji`
--
ALTER TABLE `bayar_gaji`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `departemen`
--
ALTER TABLE `departemen`
  ADD PRIMARY KEY (`id_departemen`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`ID`) USING BTREE;

--
-- Indexes for table `hak_akses`
--
ALTER TABLE `hak_akses`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `hari_libur`
--
ALTER TABLE `hari_libur`
  ADD PRIMARY KEY (`ID`) USING BTREE;

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id_info`);

--
-- Indexes for table `ip`
--
ALTER TABLE `ip`
  ADD PRIMARY KEY (`id_ip`);

--
-- Indexes for table `izin`
--
ALTER TABLE `izin`
  ADD PRIMARY KEY (`ID`) USING BTREE;

--
-- Indexes for table `kasbon`
--
ALTER TABLE `kasbon`
  ADD PRIMARY KEY (`id_kasbon`) USING BTREE;

--
-- Indexes for table `menuadmin`
--
ALTER TABLE `menuadmin`
  ADD PRIMARY KEY (`idmenu`) USING BTREE;

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slip_gaji`
--
ALTER TABLE `slip_gaji`
  ADD PRIMARY KEY (`id_slip`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absen`
--
ALTER TABLE `absen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bayar_gaji`
--
ALTER TABLE `bayar_gaji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hak_akses`
--
ALTER TABLE `hak_akses`
  MODIFY `id_level` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `hari_libur`
--
ALTER TABLE `hari_libur`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ip`
--
ALTER TABLE `ip`
  MODIFY `id_ip` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `izin`
--
ALTER TABLE `izin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kasbon`
--
ALTER TABLE `kasbon`
  MODIFY `id_kasbon` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menuadmin`
--
ALTER TABLE `menuadmin`
  MODIFY `idmenu` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `slip_gaji`
--
ALTER TABLE `slip_gaji`
  MODIFY `id_slip` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=443;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
